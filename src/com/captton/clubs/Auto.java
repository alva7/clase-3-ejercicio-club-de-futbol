package com.captton.clubs;

public class Auto {
private String nombre;
private String marca;
private int patente;

public Auto(String nombre, String marca, int patente) {
	super();
	this.nombre = nombre;
	this.marca = marca;
	this.patente = patente;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getMarca() {
	return marca;
}

public void setMarca(String marca) {
	this.marca = marca;
}

public int getPatente() {
	return patente;
}

public void setPatente(int patente) {
	this.patente = patente;
}




}
