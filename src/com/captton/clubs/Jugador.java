package com.captton.clubs;

import java.util.ArrayList;

public class Jugador {
private String nombre;
private double salario;
private ArrayList<Auto> autos;
private Club club;
private static int tarjetasAmarillas;

public Jugador(String nombre, int salario) {
	super();
	this.nombre = nombre;
	this.salario = salario;
	autos = new ArrayList<Auto>();
}

public Jugador(String nombre, int salario, Club club) {
	super();
	this.nombre = nombre;
	this.salario = salario;
	this.club = club;
	autos = new ArrayList<Auto>();
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public double getSalario() {
	return salario;
}

public void setSalario(int salario) {
	this.salario = salario;
}

public Club getClub() {
	return club;
}

public void setClub(Club club) {
	this.club = club;
}

public void comprarAuto(Auto aut) {
	this.autos.add(aut);
}
public void marcoGol() {
	this.salario=this.salario+(this.salario*0.05);
}
public void tarjetaRoja() {
	this.salario=this.salario-(this.salario*0.05);
	this.tarjetasAmarillas=this.tarjetasAmarillas+2;
}

public static int getTarjetasAmarillas() {
	return tarjetasAmarillas;
}

public void listarAutos() {
	System.out.println("La lista de autos poseidos de "+getNombre()+" es:\n");
	for (Auto aut: this.autos) {
	System.out.println("Nombre: "+aut.getNombre());
	System.out.println("Marca: " + aut.getMarca());
	System.out.println("Patente: " + aut.getPatente()+"\n");
	}
}
}
