package com.captton.clubs;

import java.util.ArrayList;

public class Club {
	private String nombre;
	private String liga;
	private ArrayList<Jugador> jugadores;
	
	public Club(String nombre, String liga) {
		super();
		this.nombre = nombre;
		this.liga = liga;
		jugadores = new ArrayList<Jugador>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}
	
	public void ficharJugador(Jugador jug) {
			this.jugadores.add(jug);

		}
	
	
	public void listarJugadores() {
		System.out.println("La plantilla de jugadores actual es: ");
		for (Jugador jug: this.jugadores) {
		System.out.println("Nombre: " + jug.getNombre());
		System.out.println("Salario: " + jug.getSalario()+"\n");
	}
	}


}
