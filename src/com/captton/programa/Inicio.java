package com.captton.programa;

import com.captton.clubs.*;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Club clu1 = new Club("Chelsea F.C.", "Barclays Premier League");
		Jugador jug1 = new Jugador("Romario",100000);
		Jugador jug2 = new Jugador("William",150000);
		Jugador jug3 = new Jugador("Eden Hazard",300000);
		Jugador jug4 = new Jugador("Drogba",200000);
		Jugador jug5 = new Jugador("Diego",100000);
		Auto auto1 = new Auto("Accent", "Hyundai", 00256);
		Auto auto2 = new Auto("Fiesta", "Ford", 00350);
		Auto auto3 = new Auto("Mustang GT", "Ford", 00256);
		jug1.comprarAuto(auto1);
		jug1.comprarAuto(auto3);
		jug2.comprarAuto(auto2);
		jug1.marcoGol();
		jug1.marcoGol();
		jug2.tarjetaRoja();
		jug3.tarjetaRoja();
		clu1.ficharJugador(jug1);
		clu1.ficharJugador(jug2);
		clu1.ficharJugador(jug3);
		clu1.ficharJugador(jug4);
		clu1.ficharJugador(jug5);
		clu1.listarJugadores();
		jug1.listarAutos();
		jug2.listarAutos();
		System.out.println("La cantidad de tarjetas amarillas de todo el plantel es: " + jug2.getTarjetasAmarillas());
		
	}

}
